const fs = require('fs');
const pug = require('pug');
const pretty = require('pretty');
const hound = require('hound');
const liveServer = require('live-server');
const config = JSON.parse(fs.readFileSync("./config.json"));
let locals = JSON.parse(fs.readFileSync(`${config.watchDirectory}/locals.json`));

let fsTimeout = false;

const walk = dir => {
    let results = [];
    let list = fs.readdirSync(dir);
    list.forEach(file => {
        file = dir + '/' + file;
        let stat = fs.statSync(file);
        if (stat && stat.isDirectory()) { 
            /* Recurse into a subdirectory */
            results = results.concat(walk(file));
        } else { 
            /* Is a file */
            results.push(file);
        }
    });
    return results;
}

// Compiles a .pug file and returns the HTML
const compile = relativeFilePath => {
    const compiled = pug.renderFile(config.watchDirectory + "/" + relativeFilePath, locals);

    if(config.pretty) return pretty(compiled);
    return compiled;
}

// If the directory of the specified file does not exist, create it
const createDirIfNotExists = file => {
    let parts = file.split("/");
    let dir = parts.slice(0, parts.length - 1).join('/'); // Get all parts except the last one and join them

    if(!fs.existsSync(dir) || !fs.lstatSync(dir).isDirectory()) {
        fs.mkdirSync(dir);
    }

}

let watcher = hound.watch(config.watchDirectory);

const change = file => {
    // Get the path inside the watch dir
    // e.g. ./src/index.pug => index.pug
    let relativeFilePath = file.substr(config.watchDirectory.length + 1);

    // If locals.json has changed, reload it
    if(relativeFilePath === "locals.json") {
        locals = JSON.parse(fs.readFileSync(file, 'utf8'));
    }

    if(!fsTimeout) {
        fsTimeout = true;
        setTimeout(() => fsTimeout = false, 1000);
        let files = walk(config.watchDirectory);

        for(let watchFile of files ) {
            let relativeFilePath = watchFile.substr(config.watchDirectory.length + 1);
            let isFileIgnored = false;
            // Ignore files from config.ignoredFiles
            for(let ignored of config.ignoredFiles) {
                if(new RegExp(ignored).test(relativeFilePath)) isFileIgnored = true;
            }

            if(/\.pug$/.test(watchFile) && !isFileIgnored) {
                let html = compile(relativeFilePath);
                let outputFileName = relativeFilePath.replace(/\.pug$/, '.html');
                let absoluteOutputFileName = config.outputDirectory + "/" + outputFileName;
                createDirIfNotExists(absoluteOutputFileName);
                fs.writeFileSync(absoluteOutputFileName, html);
                console.log(`Compiled and saved ${relativeFilePath} as ${outputFileName}`);
            }
        }
    }
};

watcher.on('create', change);
watcher.on('change', change);
watcher.on('delete', change);

liveServer.start(config);
console.log(`Listening for changes in ${config.watchDirectory}`);